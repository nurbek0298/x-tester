import farma from '@/data/tests/Фармакология.json'
import ftiz from '@/data/tests/Фтизиатрия.json'
import infec from '@/data/tests/Инфекционные болезни.json'
import infecMoodle from '@/data/tests/Инфекционные болезни(Moodle).json'

export const fetchTests = () => [farma, ftiz, infec, infecMoodle]
