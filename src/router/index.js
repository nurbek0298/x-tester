import { createRouter, createWebHistory } from 'vue-router'
import TestingPage from '../pages/TestingPage.vue'
import ResultPage from '../pages/ResultPage.vue'
import SettingsPage from '../pages/SettingsPage.vue'
import SelectTestPage from '../pages/SelectTestPage.vue'

const routes = [
  {
    path: '/',
    name: 'SelectTestPage',
    component: SelectTestPage,
  },
  {
    path: '/settings/:testId',
    name: 'SettingsPage',
    component: SettingsPage,
  },
  {
    path: '/testing/:testId/:questionNumber',
    name: 'TestingPage',
    component: TestingPage,
  },
  {
    path: '/result/:testId',
    name: 'ResultPage',
    component: ResultPage,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
